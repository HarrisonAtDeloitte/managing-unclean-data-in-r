
# Session Information ---------------------------------------------------------------------------------------------

# Title:      Managing Unclean Data in R
# Script:     01-importing-data.R
# Date:       August 31, 2021
# Presenter:  Harrison Jones, hajones@deloitte.ca
# Moderator:  Bryan Liu, shinliu@deloitte.ca


# Setup -----------------------------------------------------------------------------------------------------------

rm(list = ls())

library(readr) # to import data more efficiently than base R functions
library(data.table) # to use fread()
library(purrr) # for map()
library(dplyr) # generally used for data management

# change this to your local directory
path <- '~/Conferences/2021-08 SOA Life Meeting/managing-unclean-data-in-r'


# Different functions for the same purpose ------------------------------------------------------------------------

# when importing a clean dataset, you should receive 0 parsing errors
# both read.csv or read_csv will be sufficient (i.e. no issues importing)
# generally speaking, read_csv is 2-3x faster than read.csv and fread is 2-3x faster than read_csv
system.time(mortality_tidyverse <- read_csv(paste0(path, '/1. Data/mortality_clean.csv'), show_col_types = F))
system.time(mortality_base <- read.csv(paste0(path, '/1. Data/mortality_clean.csv')))
system.time(mortality_datatable <- fread(paste0(path, '/1. Data/mortality_clean.csv')))

mortality_unclean_tidyverse <- read_csv(paste0(path, '/1. Data/mortality_unclean.csv'), guess_max = 10, show_col_types = F)
mortality_unclean_base <- read.csv(paste0(path, '/1. Data/mortality_unclean.csv'))
mortality_unclean_datatable <- fread(paste0(path, '/1. Data/mortality_unclean.csv'))

# 1st generation readr package has the parsing error messages as more obvious
mortality_unclean_tidyverse_gen1 <- with_edition(1, read_csv(paste0(path, '/1. Data/mortality_unclean.csv')))

# 2nd generation readr package uses lazy reading, which means the errors are not immediately apparent
#   - they only show up when you try and use the entire data
mortality_unclean_tidyverse %>% filter(age > 50)
problems(mortality_unclean_tidyverse)

# read.csv and fread import the policy_id and age columns as characters and read_csv imports them as numeric
map(mortality_unclean_tidyverse %>% select(policy_id, age), class)
# read.csv will sometimes import strings as factors, but this depends on your default setting in R
# default.stringsAsFactors()
map(mortality_unclean_base %>% select(policy_id, age), class)
map(mortality_unclean_datatable %>% select(policy_id, age), class)


# Parsing errors --------------------------------------------------------------------------------------------------

# Alternative #1: set the guess_max parameter to a higher number
#   -> this will assess the column types automatically for the first 200k rows
mortality_unclean_tidyverse <- read_csv(
  paste0(path, '/1. Data/mortality_unclean.csv'), 
  guess_max = 200000, 
  show_col_types = F
)

# Alternative #2: define the column types while you import the data
mortality_unclean_tidyverse <- read_csv(
  paste0(path, '/1. Data/mortality_unclean.csv'), 
  col_types = list(
    col_character(), # policy_id
    col_character(), # age
    col_character(), # gender
    col_character(), # smoker_status
    col_character(), # fsa
    col_character(), # province
    col_double()     # death
  ), 
  show_col_types = F
)


